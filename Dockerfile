FROM php:8-apache

# Install required packages
RUN apt-get update && \
    apt-get install -y \
    zlib1g-dev \
    libzip-dev \
    curl \
    libicu-dev \
    ca-certificates \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Enable necessary PHP extensions
RUN docker-php-ext-install zip intl pdo_mysql

# Copy Apache configuration
COPY ./docker/000-default.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod ssl && a2enmod rewrite
# RUN a2enmod ssl && a2enmod rewrite && service apache2 restart quand je mets j'ai une erreur TABLE NAME


# Set correct permissions
RUN chmod -R 755 /var/www/html

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install wait-for-it
RUN curl -o /usr/local/bin/wait-for-it https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh && \
    chmod +x /usr/local/bin/wait-for-it

# Installe Node.js 20.x (ou une version ultérieure)
# RUN apt-get update \
#     && apt-get install -y \
#     gnupg \
#     && curl -sL https://deb.nodesource.com/setup_20.x | bash - \
#     && apt-get install -y nodejs

# Installe npm séparément
# RUN apt-get install -y npm

# Installe Yarn
# RUN npm install -g yarn

# Ajoute le chemin d'installation de npm au PATH
# ENV PATH="${PATH}:/usr/local/lib/node_modules/npm/bin"

WORKDIR /var/www/html

ARG COMPOSER_ALLOW_SUPERUSER=1

# Copy composer files
COPY composer.* symfony.* ./
RUN mkdir ./bin
COPY bin/console ./bin
RUN chmod 755 -R ./bin/
COPY .env ./

# CMD tail -f
RUN composer install --no-dev --no-scripts --optimize-autoloader --no-progress --ignore-platform-reqs

# Copier les sous répertoires de webapp2_1 dans /var/www/html
COPY . ./

# RUN yarn

# DEBUG
# CMD tail -f

# CMD instruction for your application

# v1
CMD composer install --no-progress && \
    wait-for-it -t 120 mysql:3306 -- php bin/console doctrine:migration:migrate -n --allow-no-migration && \
    apache2-foreground

# CMD composer install --no-progress && \
#     apache2-foreground
# apache2-foreground && \
# yarn encore dev --watch

EXPOSE 80
